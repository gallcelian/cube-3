# Gitlab Runner

Pour tester les pipelines sans avoir à push :

Pré-requis : avoir setup un `gitlab-runner` en suivant la [doc de gitlab](https://docs.gitlab.com/runner/install/).

Il n'est pas possible de `gitlab-runner exec docker <job_name>` sans être à la racine du projet git. Comme les configuration ci/cd sont séparées dans les différents services, `gitlab-runner` ne trouve pas les différents configuration s'il est exécuté depuis la racine du projet. Il faut donc lui spécifier quel fichier `.gitlab-ci.yml` utiliser :

```shell
gitlab-runner exec docker --cicd-config-file services/catalog/cicd/.gitlab-ci.yml docker-build
```

## Remarque concernant docker push

Si vous décidez de pousser votre image, seule le dernier stage est push.

Par exemple, si vous ajoutez le binaire `composer` dans `php_base`, que vous l'appelez dans `php_dev`, vous pourrez le faire au moment de build mais pas sur l'image finale en faisant `docker pull`. Pour contourner ce comportement, il faut utiliser `docker compose` à l'intérieur de la pipeline car il est possible de cibler un stage avec. 
