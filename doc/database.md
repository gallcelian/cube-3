# Base de données

## Structure

La structure de la base de données s'inspire des données proposées par [https://dummyjson.com/docs/products](https://dummyjson.com/docs/products) afin de faciliter le développement du front du moment que le back n'est pas prêt.
