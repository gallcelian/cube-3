# Guide de déploiement

Pour déployer :
- *Optionnel* : Réalisez les [instructions de cette section](https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html#use-docker-commands) de la documentation gitlab afin de vous authentifier au registry et pouvoir pull les images.
- `ln -s docker/apache2/api.cube-3.webpocketgrain.fr.conf /etc/apache2/sites-available/api.cube-3.webpocketgrain.fr.conf`
- `ln -s docker/apache2/cube-3.webpocketgrain.fr.conf /etc/apache2/sites-available/cube-3.webpocketgrain.fr.conf`
- `a2ensite api.cube-3.webpocketgrain.fr.conf`
- `a2ensite cube-3.webpocketgrain.fr.conf`
- `sudo docker compose build`.
- `sudo docker compose push`.
- Lancez la pipeline depuis l'interface gitlab.

## Déploiement continu

Le déploiement continu a été mis en place en suivant ce tuto : https://www.digitalocean.com/community/tutorials/how-to-set-up-a-continuous-deployment-pipeline-with-gitlab-on-ubuntu#step-3-creating-a-deployment-user