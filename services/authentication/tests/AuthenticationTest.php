<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;

class AuthenticationTest extends ApiTestCase
{
    public function testAuthentication(): void
    {
        $dummy = "yes";
        $this->assertSame("yes", $dummy);
    }
}