# The different stages of this Dockerfile are meant to be built into separate images
# https://docs.docker.com/develop/develop-images/multistage-build/#stop-at-a-specific-build-stage
# https://docs.docker.com/compose/compose-file/#target

#####
# PHP
#####
FROM php:8.2.12-fpm-alpine AS php_base

WORKDIR /home/www-data

# S'assure que le script s'arrête en cas d'erreur, signale les variables non définies et affiche chaque commande exécutée. Facilite le débogage.
RUN set -eux

# Embeds sources inside the container
COPY --chown=www-data:www-data . .

RUN chmod +x bin/console

# persistent / runtime dependencies
RUN apk add --no-cache acl fcgi file gettext git

# composer can't be installed with apk because of the php version linked to composer in the alpine repositories (8.1)
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN mv composer.phar /usr/local/bin/composer
RUN php -r "unlink('composer-setup.php');"

# php extensions installer: https://github.com/mlocati/docker-php-extension-installer
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions

RUN install-php-extensions pdo_pgsql pgsql

# Dev PHP image
FROM php_base AS php_dev

ENV APP_ENV=dev XDEBUG_MODE=off

RUN install-php-extensions xdebug

# symfony-cli
RUN apk add --no-cache bash
RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.alpine.sh' | bash
RUN apk add --no-cache symfony-cli

CMD ["sh", "-c", "composer i --no-cache --prefer-dist --no-scripts --no-progress && symfony serve"]
