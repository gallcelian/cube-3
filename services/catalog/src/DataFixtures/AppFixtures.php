<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create("fr_FR");
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 20; $i++) {
            $product = new Product();
            $product
                ->setTitle(ucfirst($this->faker->word()))
                ->setPrice($this->faker->numberBetween(0, 1000))
                ->setDescription($this->faker->paragraph())
                ->setBrand(ucfirst($this->faker->word()))
                ->setStock($this->faker->randomDigit())
                ->setCategory(ucfirst($this->faker->word()));
            $manager->persist($product);
        }

        $manager->flush();
    }
}
