<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Vérifie que le token est valide auprès sur service authentication pour chacune des requêtes entrantes.
 * Si le token est valide, alors la requête est forwardée vers le service correspondant, sinon la réponse du
 * service authentication est retournée.
 */
class IncomingRequestsSubscriber implements EventSubscriberInterface
{
    /**
     * @throws TransportExceptionInterface
     */
    public function onKernelRequest(RequestEvent $event): ResponseInterface
    {
        $request = $event->getRequest();
        $client = HttpClient::create();

        if ($request->getPathInfo() !== '/login' && $request->getPathInfo() !== '/register' && $request->getBasePath() == '/auth') {
            $response = $client->request('GET', 'http://api-authentication:8000/check', [
                'headers' => [
                    'Authorization' => $request->headers->get('Authorization'),
                ],
            ]);
            if ($response->getStatusCode() !== 200) {
                return $response;
            }
        }
        /**
         * Redirecting to api-catalog.
         */
        if ($request->getBasePath() == '/catalog') {
            $path = $request->getPathInfo();
            $uri = `http://api-catalog$path`;
            return $client->request($request->getMethod(), $uri);
        }

        throw new NotFoundHttpException("Route introuvable.");
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }
}