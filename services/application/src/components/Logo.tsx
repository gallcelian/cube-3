import { ReactElement } from 'react'
import { Group, Text } from '@mantine/core'
import { IconShirtSport} from '@tabler/icons-react'
import { Pill } from '@mantine/core'

export function Logo(): ReactElement {
    return (
        <Group gap={'sm'}>
            <IconShirtSport />
            <Text fw={'bold'}>Breizhsport</Text>
            <Pill>v0.0.1</Pill>
        </Group>
    )
}
