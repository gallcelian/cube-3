import { ActionIcon, useMantineColorScheme, useComputedColorScheme, Flex } from '@mantine/core'
import { IconMoon, IconSun } from '@tabler/icons-react'
import { ReactElement } from 'react'

export function SwitchThemeButton(): ReactElement {
    const { setColorScheme } = useMantineColorScheme()
    const computedColorScheme = useComputedColorScheme('light', { getInitialValueInEffect: true })

    return (
        <Flex justify="center">
            <ActionIcon
                onClick={() => setColorScheme(computedColorScheme === 'light' ? 'dark' : 'light')}
                variant="default"
                aria-label="Changer la couleur du thème"
            >
                {computedColorScheme === 'light' ? <IconSun /> : <IconMoon />}
            </ActionIcon>
        </Flex>
    )
}
