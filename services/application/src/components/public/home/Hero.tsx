import { ReactElement } from 'react'
import { Flex, Group, Image, Stack, Text, Title } from '@mantine/core'
import HeroImageLayerZero from '../../../assets/public/home/hero_image_layer_0.svg'
import HeroImage from '../../../assets/public/home/hero_image.png'
import css from '../../../styles/public/home/Hero.module.css'

export function Hero(): ReactElement {
    return (
        <Group
            w={'100%'}
            px={'xl'}
            justify={'center'}
        >
            <Group
                justify={'space-between'}
                maw={'1136px'}
                mx={'auto'}
                h={'84vh'}
                align={'center'}
            >
                <Stack w={{ base: '100%', lg: '46%' }}>
                    <Title order={1}>Bienvenue AU CESI</Title>
                    <Text
                        lh={'xl'}
                        ta={'justify'}
                    >
                        Votre magasin de sport favori, retrouvez des milliers de référence et des offres toute l'année sur les équipements de football, hockey, boxe, course à pied...
                    </Text>
                </Stack>
                <Flex
                    w={'526px'}
                    h={'488px'}
                    pos={'relative'}
                    display={{ base: 'none', lg: 'flex' }}
                >
                    <Flex
                        top={'0'}
                        left={'0'}
                        pos={'absolute'}
                    >
                        <Image src={HeroImageLayerZero} />
                    </Flex>
                    <Flex
                        top={'0'}
                        left={'0'}
                        pos={'absolute'}
                    >
                        <Image
                            src={HeroImage}
                            className={css.heroimage}
                        />
                    </Flex>
                </Flex>
            </Group>
        </Group>
    )
}
