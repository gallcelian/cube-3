import {ReactElement} from 'react'
import {Text, Card, Group, Button, Badge, Image} from '@mantine/core'
import ProductImage from '../../../../assets/public/home/product_image.png'

export interface Product {
    id: number,
    title: string,
    description: string,
    price: number,
    stock: number,
    brand: string,
    category: string
}

export interface HydraCollection<T extends any> {
    'hydra:member': Array<HydraItem<T>>
    'hydra:totalItems': number
}

export type HydraMetadata = {
    createdBy?: string
    updatedBy?: string
    createdAt?: Date
    updatedAt?: Date
}

export type HydraItem<T extends any> = T &
    Partial<HydraMetadata> & {
    '@id': string
    id: number | string
}

export function ProductTile(props: { product: Product }): ReactElement {

    return (
        <Card shadow="sm" padding="lg" my={'lg'} radius="md" withBorder w={'32%'}>
            <Card.Section>
                <Image
                    src={ProductImage}
                />
            </Card.Section>

            <Group justify="space-between" mt="md" mb="xs">
                <Text fw={500}>{props.product.title}</Text>
                <Badge color="blue">{props.product.price}€</Badge>
            </Group>

            <Text size="sm" c="dimmed">
                {props.product.description}
            </Text>

            <Button color="blue" fullWidth mt="md" radius="md">
                Acheter
            </Button>
        </Card>
    )
}
