import {ReactElement, useEffect, useState} from 'react'
import {Flex, Group, Skeleton, Title, useComputedColorScheme} from '@mantine/core'
import {HydraCollection, HydraItem, Product, ProductTile} from "./blurb/ProductTile.tsx";
import CatalogService from "../../../services/catalog/CatalogService.tsx";

export function Blurb(): ReactElement {
    const computedColorScheme: string = useComputedColorScheme('light', {getInitialValueInEffect: true})

    const [loading, setLoading] = useState<boolean>(true)
    const [productTiles, setProductTiles] = useState<ReactElement[]>([])

    useEffect(() => {
        const fetchProducts = async () => {
            let products: HydraCollection<Product> = await CatalogService.fetchProducts() as HydraCollection<Product>;
            let productTiles: ReactElement[] = []
            for (let i: number = 0; i < products["hydra:totalItems"] - 2; i++) {
                let product: HydraItem<Product> = products['hydra:member'][i]
                productTiles.push(<ProductTile key={product.id} product={product}/>)
            }
            setProductTiles(productTiles);
        };

        fetchProducts().then(() => setLoading(false));
    }, [])

    return (
        <Group
            w={'100%'}
            bg={computedColorScheme === 'light' ? 'var(--mantine-color-gray-1)' : 'var(--mantine-color-gray-9)'}
            p={'xl'}
            justify={'center'}
        >
            <Group
                maw={'1136px'}
                justify={'space-between'}
            >
                {!loading ?
                    <Group w={{base: '100%'}}>
                        <Title order={1}>Notre sélection de produits</Title>
                        <Flex direction={'row'} wrap={'wrap'} w={'100%'} justify={'space-between'}>
                            {productTiles}
                        </Flex>
                    </Group>
                    : <Skeleton/>
                }
            </Group>
        </Group>
    )
}
