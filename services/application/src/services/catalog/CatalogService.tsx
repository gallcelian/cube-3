import { DOMAIN, PORT, PROTOCOL } from '../constants.ts'
import { notifications } from '@mantine/notifications'
import {HydraCollection, Product} from "../../components/public/home/blurb/ProductTile.tsx";

class CatalogService {
    SERVICE_URL: string = `${PROTOCOL}://${DOMAIN}:${PORT}`

    async fetchProducts(): Promise<HydraCollection<Product> | []> {
        interface FetchPatientsError {
            error: string
        }

        const options: RequestInit = {
            method: 'GET',
            headers: new Headers({
                'Content-Type': 'application/json',
            }),
        }
        let response: Response = await fetch(`${this.SERVICE_URL}/products`, options)
        if (response.ok) {
            return response.json();
        } else {
            let error: FetchPatientsError = await response.json()
            notifications.show({
                title: 'Échec de la récupération des produits.',
                message: error.error,
                color: 'red',
                radius: 'md',
                withCloseButton: false,
            })
            return []
        }
    }
}

export default new CatalogService()
