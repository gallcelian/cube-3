export interface Preferences {
    id?: number
    email_notifications: boolean
    sms_notifications: boolean
    terms_of_services?: boolean
}
