export interface Users {
    id?: number
    surname?: string
    name?: string
    email?: string
    password?: string
    preferences?: number
    birth_date?: string
    parent?: boolean
    created_at?: string
}
