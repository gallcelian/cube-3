import { DOMAIN, PORT, PROTOCOL } from './constants.ts'
import { notifications } from '@mantine/notifications'
import { Users } from './users/models/Users.ts'

export interface LoginBody {
    email: string
    password: string
}

interface TokenResponse {
    message: string
    token: string
}

interface RegisterError {
    error: string
    description: string
}

interface LoginError {
    error: string
}

class AuthenticationService {
    TOKEN_KEY: string = 'token'
    SERVICE_URL: string = `${PROTOCOL}://${DOMAIN}:${PORT}/auth`

    async register(body: Users): Promise<void> {
        const url: string = `${this.SERVICE_URL}/register`

        const options: RequestInit = {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
            }),
            body: JSON.stringify(body),
        }

        let response: Response = await fetch(url, options)

        if (response.ok) {
            let success: TokenResponse = await response.json()
            notifications.show({
                title: success.message,
                message: success.token,
                radius: 'md',
                withCloseButton: false,
            })
        } else {
            let error: RegisterError = await response.json()
            notifications.show({
                title: error.error,
                message: error.description,
                color: 'red',
                radius: 'md',
                withCloseButton: false,
            })
        }
    }

    async login(body: LoginBody): Promise<string | undefined> {
        const url: string = `${this.SERVICE_URL}/login`

        const options: RequestInit = {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
            }),
            body: JSON.stringify(body),
        }

        let response: Response = await fetch(url, options)

        if (response.ok) {
            let success: TokenResponse = await response.json()
            return success.token
        } else {
            let error: LoginError = await response.json()
            notifications.show({
                title: 'Accès refusé',
                message: error.error,
                color: 'red',
                radius: 'md',
                withCloseButton: false,
            })
            return undefined
        }
    }

    logout(): void {
        localStorage.removeItem(this.TOKEN_KEY)
    }
}

// creates a singleton
export default new AuthenticationService()
