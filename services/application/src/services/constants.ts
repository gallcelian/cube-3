export const PROTOCOL: string = import.meta.env.VITE_AUTHENTICATION_SERVICE_PROTOCOL
export const DOMAIN: string = import.meta.env.VITE_AUTHENTICATION_SERVICE_DOMAIN
export const PORT: string = import.meta.env.VITE_AUTHENTICATION_SERVICE_PORT
