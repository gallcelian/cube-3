import '@mantine/core/styles.css'
import '@mantine/dates/styles.css'
import '@mantine/notifications/styles.css'
import { RouterProvider } from 'react-router-dom'
import { AppRouter } from './AppRouter.tsx'
import { ReactElement } from 'react'

export function App(): ReactElement {
    return <RouterProvider router={AppRouter} />
}
