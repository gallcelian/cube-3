import { Context, createContext, useEffect, useState } from 'react'
import AuthenticationService from '../services/AuthenticationService.tsx'

export interface UserContextAttributes {
    id: number | undefined
    exp: number | undefined
    ss: string | undefined
}

export const UserContext: Context<UserContextAttributes | undefined> = createContext<UserContextAttributes | undefined>(undefined)

export const UserProvider = ({ children }: any) => {
    const [id, setId] = useState<number | undefined>(undefined)
    const [exp, setExp] = useState<number | undefined>(undefined)
    const [ss, setSs] = useState<string | undefined>(undefined)

    useEffect(() => {
        let token: string | null = localStorage.getItem(AuthenticationService.TOKEN_KEY)
        if (token) {
            let tokenBody: string = token.split('.')[1]
            let decodedToken: UserContextAttributes = JSON.parse(atob(tokenBody))

            if (decodedToken.id) {
                setId(decodedToken.id)
            }
            if (decodedToken.exp) {
                setExp(decodedToken.exp)
            }
            if (decodedToken.ss) {
                setSs(decodedToken.ss)
            }
        }
    })

    return <UserContext.Provider value={{ id, exp, ss }}>{children}</UserContext.Provider>
}
