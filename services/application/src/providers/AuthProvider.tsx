import { Context, createContext, useState } from 'react'
import AuthenticationService from '../services/AuthenticationService.tsx'

export interface AuthContextAttributes {
    token: string | undefined
    login: (token: string) => void
    logout: () => void
}

export const AuthContext: Context<AuthContextAttributes | undefined> = createContext<AuthContextAttributes | undefined>(undefined)

export const AuthProvider = ({ children }: any) => {
    const [token, setToken] = useState(localStorage.getItem(AuthenticationService.TOKEN_KEY) || undefined)

    function login(token: string): void {
        setToken(token)
        localStorage.setItem(AuthenticationService.TOKEN_KEY, token)
    }

    function logout(): void {
        setToken(undefined)
        localStorage.removeItem(AuthenticationService.TOKEN_KEY)
    }

    return <AuthContext.Provider value={{ token, login, logout }}>{children}</AuthContext.Provider>
}
