import { useContext } from 'react'
import { AuthContext, AuthContextAttributes } from '../providers/AuthProvider.tsx'

export function useAuth(): AuthContextAttributes | undefined {
    return useContext(AuthContext)
}
