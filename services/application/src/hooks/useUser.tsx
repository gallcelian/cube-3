import { useContext } from 'react'
import { UserContext, UserContextAttributes } from '../providers/UserProvider.tsx'

export function useUser(): UserContextAttributes | undefined {
    return useContext(UserContext)
}
