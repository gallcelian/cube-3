import React from 'react'
import ReactDOM from 'react-dom/client'
import { App } from './App.tsx'
import { theme } from './theme.ts'
import { MantineProvider } from '@mantine/core'
import { Notifications } from '@mantine/notifications'
import { AuthProvider } from './providers/AuthProvider.tsx'
import { UserProvider } from './providers/UserProvider.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <AuthProvider>
            <UserProvider>
                <MantineProvider
                    defaultColorScheme={'auto'}
                    theme={theme}
                >
                    <Notifications />
                    <App />
                </MantineProvider>
            </UserProvider>
        </AuthProvider>
    </React.StrictMode>,
)
