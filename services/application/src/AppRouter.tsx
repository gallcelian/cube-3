import { createBrowserRouter, createRoutesFromElements, Route } from 'react-router-dom'
import { Layout as RootLayout } from './pages/Layout.tsx'
import { Home as PublicHome } from './pages/public/home/Home.tsx'
import { Layout as PublicLayout } from './pages/public/Layout.tsx'

export const AppRouter = createBrowserRouter(
    createRoutesFromElements(
        <Route
            path={'/'}
            element={<RootLayout />}
        >
            <Route element={<PublicLayout />}>
                <Route
                    index
                    element={<PublicHome />}
                />
            </Route>
        </Route>,
    ),
)
