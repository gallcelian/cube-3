import { createTheme } from '@mantine/core'

/** https://mantine.dev/theming/theme-object/ */
export const theme = createTheme({
    fontFamily: 'Open Sans, sans-serif',

    /** same as figma gradient but auto calculated : https://mantine.dev/theming/colors/ */
    primaryColor: 'blue',
})
