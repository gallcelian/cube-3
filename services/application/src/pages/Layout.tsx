import { Outlet } from 'react-router-dom'
import { ReactElement } from 'react'

export function Layout(): ReactElement {
    return <Outlet />
}
