import { ReactElement } from 'react'
import { Stack } from '@mantine/core'
import { Hero } from '../../../components/public/home/Hero.tsx'
import { Blurb } from '../../../components/public/home/Blurb.tsx'
import { Footer } from '../Footer.tsx'

export function Home(): ReactElement {
    return (
        <Stack>
            <Hero />
            <Blurb />
            <Footer />
        </Stack>
    )
}
