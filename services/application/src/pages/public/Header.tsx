import {ReactElement} from 'react'
import {Anchor, Button, Group} from '@mantine/core'
import {Logo} from '../../components/Logo.tsx'

interface HeaderLink {
    label: string
    link: string
}

const links: HeaderLink[] = [
    {label: 'Accueil', link: '#'},
    {label: 'Contact', link: '#'},
]

export function Header(): ReactElement {
    const navigationItems: ReactElement[] = links.map((link: HeaderLink) => (
        <Anchor
            key={link.label}
            href={link.link}
            c={'var(--mantine-color-text)'}
        >
            {link.label}
        </Anchor>
    ))

    return (
        <Group
            justify={'space-between'}
            maw={'1136px'}
            mx={'auto'}
        >
            <Group gap={'xl'}>
                <Logo/>
                {navigationItems}
            </Group>
            <Group>
                <Button
                    variant={'outline'}
                    radius="md"
                >
                    S'enregistrer
                </Button>
                <Button radius="md">Se connecter</Button>
            </Group>
        </Group>
    )
}
