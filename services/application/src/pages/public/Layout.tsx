import { Outlet } from 'react-router-dom'
import { ReactElement } from 'react'
import { AppShell } from '@mantine/core'
import { Header } from './Header.tsx'

export function Layout(): ReactElement {
    return (
        <AppShell>
            <AppShell.Header
                py={'lg'}
                px={'xl'}
            >
                <Header />
            </AppShell.Header>
            <AppShell.Main>
                <Outlet />
            </AppShell.Main>
        </AppShell>
    )
}
