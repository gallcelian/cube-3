import { Group, Anchor } from '@mantine/core'
import { ReactElement } from 'react'
import { Logo } from '../../components/Logo.tsx'

interface FooterLink {
    label: string
    link: string
}

const links: FooterLink[] = [
    { label: "Conditions générales d'utilisation", link: '#' },
    { label: 'Politique de confidentialité', link: '#' },
    { label: 'Politique de traitement des données', link: '#' },
]

export function Footer(): ReactElement {
    const navigationItems: ReactElement[] = links.map((link: FooterLink) => (
        <Anchor
            c="dimmed"
            key={link.label}
            href={link.link}
            size={'sm'}
        >
            {link.label}
        </Anchor>
    ))

    return (
        <Group
            w={'100%'}
            p={'xl'}
            justify={'center'}
        >
            <Group
                justify={'space-between'}
                maw={'1136px'}
                w={'100%'}
                mx={'auto'}
                align={'center'}
            >
                <Logo />
                <Group>{navigationItems}</Group>
            </Group>
        </Group>
    )
}
