# Cube 3

Ce projet contient le POC à réaliser pour le Cube 3 MAALSI.

## Guide de démarrage

1. Copiez `.env.pgsql.catalog.dist` vers `.env.pgsql.catalog.local` et renseignez les variables. Référez vous à [la documentation de PostGreSQL sur Dockerhub](https://hub.docker.com/_/postgres/#:~:text=have%20found%20useful.-,Environment%20Variables,-The%20PostgreSQL%20image) pour obtenir de plus amples informations sur la fonction de chacune des variables.
2. Idem pour `.env.pgsql.authentication.dist`.
3. Copiez `services/authentication/.env` vers `services/authentication/.env.local` et renseignez les variables.
4. Idem pour `services/catalog/.env`.
5. Idem pour `services/gateway/.env`.
6. Lancez : `docker compose up -d` depuis la racine du projet.
7. Lancez les migrations **à l'intérieur** des conteneurs `authentication` et `catalog` à l'aide de `php bin/console doctrine:schema:update --force --complete`.
8. Remplissez les tables à l'aide des données factices fournies par les fixtures **à l'intérieur** des conteneurs `authentication` et `catalog` à l'aide de `php bin/console doctrine:fixtures:load`.
9. Rendez-vous sur [http://localhost:5173](http://localhost:5173).