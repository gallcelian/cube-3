# https://github.com/nextcloud/all-in-one/blob/main/reverse-proxy.md

<VirtualHost *:80>
    ServerName api.cube-3.webpocketgrain.fr

    Redirect permanent / https://api.cube-3.webpocketgrain.fr/
    RewriteEngine on
    RewriteCond %{SERVER_NAME} =api.cube-3.webpocketgrain.fr
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>

<VirtualHost *:443>
    ServerName api.cube-3.webpocketgrain.fr

    # Reverse proxy based on https://httpd.apache.org/docs/current/mod/mod_proxy_wstunnel.html
    RewriteEngine On
    ProxyPreserveHost On
    AllowEncodedSlashes NoDecode

    ProxyPass / http://localhost:84/ nocanon
    ProxyPassReverse / http://localhost:84/

    # Prevents "Bypass invalid certificate warning" when trying to access the site via HTTP
    Header always set Strict-Transport-Security max-age=15552000

    RequestHeader set X-Real-IP %{REMOTE_ADDR}s
    RequestHeader set X-Forwarded-For %{REMOTE_ADDR}s
    RequestHeader set X-Forwarded-Proto https

    RewriteCond %{HTTP:Upgrade} websocket [NC]
    RewriteCond %{HTTP:Connection} upgrade [NC]
    RewriteCond %{THE_REQUEST} "^[a-zA-Z]+ /(.*) HTTP/\d+(\.\d+)?$"
    RewriteRule .? "ws://localhost:84/%1" [P,L]

    # Support big file uploads
    LimitRequestBody 0

    # TLS
    SSLEngine               on
    Include /etc/letsencrypt/options-ssl-apache.conf
    SSLCertificateFile /etc/letsencrypt/live/api.cube-3.webpocketgrain.fr/fullchain.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/api.cube-3.webpocketgrain.fr/privkey.pem

    # Disable HTTP TRACE method.
    TraceEnable off
    <Files ".ht*">
        Require all denied
    </Files>
</VirtualHost>